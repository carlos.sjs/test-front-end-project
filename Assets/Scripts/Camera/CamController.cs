using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    private GameObject player;
    float x;
    float y;
    float z;
    void Start(){
        player = GameObject.FindGameObjectWithTag("Player");
        x = transform.position.x-player.transform.position.x;
        y = transform.position.y-player.transform.position.y;
        z = transform.position.z-player.transform.position.z;
    }

    // Update is called once per frame
    void Update(){
        
        transform.position = new Vector3(player.transform.position.x + x,y + player.transform.position.y ,player.transform.position.z + z);
    }
}
