using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangedAttack : MonoBehaviour
{
   public bool attacking = false;
    private GameObject wepon;
    private GameObject fireP;
    public GameObject bullet;
    private float timer = 0f;

    public GunDebugger g_debugger;
    
    // Start is called before the first frame update
    void Start()
    {
        wepon = transform.GetChild(0).gameObject;
        fireP = wepon.transform.GetChild(0).gameObject;
        //fireP = wepon.transform.parent.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(attacking) Attack();
        timer += Time.deltaTime;
    }

    void Attack(){
        if(timer>=2f){
            timer = 0f;
            //g_debugger.c_time = transform.parent.GetComponent<RangedEnemyController>().expeted_collision_time;
            GameObject tempBullet = Instantiate(bullet, fireP.transform.position, transform.rotation);
            tempBullet.GetComponent<BulletController>().setParent("Enemy");
        }
    }
}
