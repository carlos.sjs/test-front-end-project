using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RangedEnemyController : MonoBehaviour
{
   [SerializeField]
    private GameObject target;
    private GameObject hands;
    private GameObject wepon;

    public EnemyRangedAttack ERA; 

    private NavMeshPath path;
    private float time = 1f;
    private Rigidbody rb;
    public float moveSpeed = 5f;
    private float rotationSpeed = 10f;
    private Vector3 mov;

    public float hp;
    private bool kback = false;
    private float tmer = 0f;
    private Vector3 kbackVector;

    public float expeted_collision_time = 0f;

    public EnemySpawnerController ESC;

    // Start is called before the first frame update
    void Start(){
        target = GameObject.FindGameObjectWithTag("Player");
        rb = gameObject.GetComponent<Rigidbody>();
        hp = 100f;
        path = new NavMeshPath();
        time = 1f;
        hands = transform.GetChild(1).gameObject;
        wepon = hands.transform.GetChild(0).gameObject;
        ERA = hands.GetComponent<EnemyRangedAttack>();
        ESC = GameObject.Find("Enemy Spawner").GetComponent<EnemySpawnerController>();
    }

    // Update is called once per frame
    void Update(){
        if(Vector3.Distance(target.transform.position, transform.position) < 50f){
            ERA.attacking = true;
            float radAngle = Mathf.Atan2(target.transform.position.z - transform.position.z, target.transform.position.x - transform.position.x);
            float v1 = Mathf.Sqrt(Mathf.Pow(target.GetComponent<Rigidbody>().velocity.x,2) + Mathf.Pow(target.GetComponent<Rigidbody>().velocity.z,2));
            float v2 = 25f*.8f;


            //Debug.Log("v1: " + v1);
            //Debug.Log("v2: " + v2);
            //Debug.Log("rad angle: " + radAngle);

            //Debug.Log("player: " + target.transform.position);
            //Debug.Log("player velocity: " + target.GetComponent<Rigidbody>().velocity);
            //Debug.Log("the bitch: " + transform.position);
            Vector2 AB =  new Vector2(target.transform.position.x - transform.position.x, target.transform.position.z - transform.position.z);
            Vector2 PlayerDirection = new Vector2(target.GetComponent<Rigidbody>().velocity.x, target.GetComponent<Rigidbody>().velocity.z);
            //Debug.Log("AB Vector:" + AB);
            //v1*=1.2f;
            float theta = Mathf.PI - Mathf.Atan2(PlayerDirection.x*AB.y - PlayerDirection.y*AB.x, PlayerDirection.x*AB.x + PlayerDirection.y*AB.y);
            if(v1!=0 && Mathf.Abs((v1 * Mathf.Sin(theta)) / v2) <= 1f){
                //float theta = Mathf.PI - Mathf.Acos(((target.GetComponent<Rigidbody>().velocity.x * AB.x) + (target.GetComponent<Rigidbody>().velocity.z * AB.y)) / ( v1 * AB.magnitude));
                float alpha = Mathf.Asin((v1 * Mathf.Sin(theta)) / v2);
                float gamma = radAngle - alpha;
                float degAngle = (-180 / Mathf.PI) * gamma + 90;

                //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, degAngle, 0), rotationSpeed * 4 * Time.deltaTime);
                transform.rotation = Quaternion.Euler(0, degAngle, 0);
                //Debug.Log("theta: " + theta);
                //Debug.Log("alpha: " + alpha);
                //Debug.Log("gamma: " + gamma);
                //Debug.Log("degAngle: " + (degAngle-90*-1));
                //Debug.Log("degAngle final: " + degAngle);

                expeted_collision_time = AB.magnitude/(v1*Mathf.Cos(theta)+v2*Mathf.Cos(alpha));
            }
            else{
                float degAngle = (-180 / Mathf.PI) * radAngle + 90;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, degAngle, 0), rotationSpeed * 2 * Time.deltaTime);
            }
        }
        else{
            ERA.attacking = false;
            if(!kback) enemy_movement();
        }
        if(hp<=0){
            ESC.NumEnemys--;
            Destroy(gameObject);
        }
        if(kback){
            tmer+=Time.deltaTime;
            if(tmer<=.2f){

                rb.velocity = kbackVector * 25f;
            }
            else kback = false;
        }
    }

    void enemy_movement(){
        time += Time.deltaTime;
        if(time >= .1f || (path.corners.Length > 1 && Vector3.Distance(transform.position, path.corners[1]) < .2f)){
            time=0f;
            NavMesh.CalculatePath(transform.position, target.transform.position, NavMesh.AllAreas, path);

            if(path.corners.Length > 1)
                mov = new Vector3((path.corners[1].x - transform.position.x), rb.velocity.y, (path.corners[1].z - transform.position.z)).normalized*moveSpeed;
            else
                mov = new Vector3(0f, 0f, 0f);
        }

        //rb.AddForce(mov * moveSpeed);
        SpeedControl(moveSpeed);
        mov.y = rb.velocity.y;
        rb.velocity = mov;
        float angle = Mathf.Atan2(rb.velocity.x, rb.velocity.z)*Mathf.Rad2Deg;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, angle, 0), rotationSpeed * Time.deltaTime);
        //transform.rotation = Quaternion.Euler(0, angle, 0);

        //Debug.DrawLine(transform.position, transform.position+rb.velocity, Color.red, Time.deltaTime);
        //Debug.DrawLine(transform.position, path.corners[1], Color.blue, Time.deltaTime);
    }

    private void SpeedControl(float speed){
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
        
        if(flatVel.magnitude > speed){
            Vector3 limitVel = flatVel.normalized * speed;
            rb.velocity = new Vector3(limitVel.x, rb.velocity.y, limitVel.z);
        }
    }

    public void hit(float dmg, bool kb){
        //Debug.Log("hit");
        hp-=dmg;
        if(kb){
            kback = true;
            kbackVector = (transform.position - target.transform.position).normalized;
            tmer = 0f;
        }
    }
}
