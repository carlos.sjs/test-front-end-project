using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawnerController : MonoBehaviour
{
    public GameObject []Enemys;
    private float range = 50.0f;
    public int NumEnemys = 0;


    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 point;
        if (RandomPoint(transform.position, range, out point) && NumEnemys<=10)
        {
            Debug.Log("Spawned");
            int EnemyType = Random.Range(0,Enemys.Length);
            GameObject tmpEnemy = Instantiate(Enemys[EnemyType],point, Quaternion.Euler(0,0,0));
            NumEnemys++;
            Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f);
        }
    }
}
