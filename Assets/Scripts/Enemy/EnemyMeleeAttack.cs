using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeAttack : MonoBehaviour
{
    public bool attacking = false;
    private GameObject wepon;
    public SwordController SC;
    private int nAttacks = 0;
    private float timer = 10f;
    
    // Start is called before the first frame update
    void Start()
    {
        wepon = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(attacking && timer >= 2f) Attack();
        if(nAttacks >= 2){
            timer = 0f;
            nAttacks = 0;
        } 
        timer += Time.deltaTime;
    }

    void Attack(){
        GetComponent<Animator>().SetTrigger("hit");
        nAttacks++;
    }

    public void OnAtack(){
        wepon.GetComponent<BoxCollider>().enabled = true; 
    }

    public void OffAtack(){
        wepon.GetComponent<BoxCollider>().enabled = false; 
    }

    public void KbOn(){
        SC.kb = true;
    }

    public void KbOff(){
        SC.kb = false;
    }
}
