using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public int maxStacked = 10;
    public SlotController[] Slots;
    public GameObject inventoryItemPrefab;

    public GameObject player;

    public int selectedSlot = -1;
    int startedPlayerGO;

    private void Awake() {
        player = GameObject.FindGameObjectWithTag("Player");
        startedPlayerGO = player.transform.childCount;
    }

    private void Start() {
        ChangeSelectedSlot(0);
    }

    private void Update() {
        if(Input.inputString != null){
            bool isNumber = int.TryParse(Input.inputString, out int number);
            if(isNumber && number > 0 && number < 7 && number!=selectedSlot+1){

                ChangeSelectedSlot(number - 1);
            }
        }
    }

    public void ChangeSelectedSlot(int newValue){
        if(selectedSlot != newValue){
            if (selectedSlot >= 0){
                Slots[selectedSlot].Deselect();
            }

            Slots[newValue].Select();
            selectedSlot = newValue;
            GetSelectedItem();
        }
    }

    public bool AddItem(ItemController item){
        for(int i=0; i < Slots.Length; i++){
            SlotController slot = Slots[i];
            InventoryItemController itemSlot = slot.GetComponentInChildren<InventoryItemController>();
            if (itemSlot != null &&
                itemSlot.item == item &&
                itemSlot.count < maxStacked &&
                itemSlot.item.stackable == true){
                itemSlot.count++;
                itemSlot.RefreshCount();
                GetSelectedItem();
                return true;
            }
        }

        for(int i=0; i < Slots.Length; i++){
            SlotController slot = Slots[i];
            InventoryItemController itemSlot = slot.GetComponentInChildren<InventoryItemController>();
            if(itemSlot == null){
                SpawnNewItem(item, slot);
                GetSelectedItem();
                return true;
            }
        }

        return false;
    }

    public void SpawnNewItem(ItemController item, SlotController slot){
        GameObject newItemGo = Instantiate(inventoryItemPrefab, slot.transform);
        InventoryItemController inventoryItem = newItemGo.GetComponent<InventoryItemController>();
        inventoryItem.InitaliseItem(item);
    }

    public ItemController SelectedItem(){
        SlotController slot = Slots[selectedSlot];
        InventoryItemController itemSlot = slot.GetComponentInChildren<InventoryItemController>();
        if (itemSlot != null){
            return itemSlot.item;
        } 
        return null;
    }

    public void GetSelectedItem(){
        ItemController recivedItem = SelectedItem();
        if(recivedItem == null){
            if(player.transform.childCount > startedPlayerGO){
                Destroy(player.transform.GetChild(player.transform.childCount - 1).gameObject);
            }
            player.transform.GetChild(0).gameObject.SetActive(true);
        }
        else{
            player.transform.GetChild(0).gameObject.SetActive(false);
            if(player.transform.childCount > startedPlayerGO){
                Destroy(player.transform.GetChild(player.transform.childCount - 1).gameObject);
            }
            if(recivedItem.type == ItemType.Wepon){
                GameObject item = Instantiate(recivedItem.ObjectModel.gameObject);
                item.transform.SetParent(player.transform);
                item.transform.SetAsLastSibling();
                item.transform.localPosition = recivedItem.Offset;
                item.transform.localRotation = Quaternion.identity;
            }

        }
    }
}
