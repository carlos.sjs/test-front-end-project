using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private Rigidbody rb;
    float Yrot;
    public Vector3 dir;
    private float timer = 0f;
    public string parent;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        float Yrot = transform.rotation.eulerAngles.y;
        Yrot *= Mathf.Deg2Rad;
        dir = new Vector3(Mathf.Sin(Yrot), 0, Mathf.Cos(Yrot));
        //Debug.Log(dir);
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 prev = transform.position;
        rb.velocity = new Vector3(dir.normalized.x * 25f, 0f, dir.normalized.z * 25f);
        SpeedControl(25f);
        //transform.position = transform.position + dir*Time.deltaTime;
        //Debug.Log("Bullet velocity: " + ((transform.position-prev)/Time.deltaTime).magnitude);
        timer += Time.deltaTime;
        if(timer>3f) Destroy(gameObject);


        //Vector3 vel = rb.velocity;
        //vel.y = 0f;
        //Debug.Log(rb.velocity);
    }

    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.CompareTag("Enemy") && parent == "Player" ){
            if(other.gameObject.GetComponent<EnemyController>()) other.gameObject.GetComponent<EnemyController>().hit(20f, false);
            else if(other.gameObject.GetComponent<RangedEnemyController>()) other.gameObject.GetComponent<RangedEnemyController>().hit(20f,false);
            Destroy(gameObject);
        }
        else if(parent == "Enemy" && other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<PlayerController>().inv==false){
            other.gameObject.GetComponent<PlayerController>().hit(20f, false, Vector3.zero);
            Destroy(gameObject);
        }
        //else Destroy(gameObject);
    }

    public void setParent (string p){
        parent = p;
    }

    private void SpeedControl(float speed){
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
        
        if(flatVel.magnitude > speed){
            Vector3 limitVel = flatVel.normalized * speed;
            rb.velocity = new Vector3(limitVel.x, rb.velocity.y, limitVel.z);
        }
    }
}
