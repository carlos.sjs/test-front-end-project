using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : MonoBehaviour
{   
    public bool kb = false;
    private void OnTriggerEnter(Collider other) {

        if(other.gameObject.CompareTag("Enemy") && gameObject.transform.parent.parent.CompareTag("Player")){
            if(other.gameObject.GetComponent<EnemyController>()) other.gameObject.GetComponent<EnemyController>().hit(50f, kb);
            else if(other.gameObject.GetComponent<RangedEnemyController>()) other.gameObject.GetComponent<RangedEnemyController>().hit(50f, kb);
        }
        else if(gameObject.transform.parent.parent.CompareTag("Enemy") && other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<PlayerController>().inv==false){
            other.gameObject.GetComponent<PlayerController>().hit(20f, kb, gameObject.transform.parent.parent.position);
        }
    }
}
