using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    public GameObject InventoryG;
    public InventoryManager inventoryManager;

    public InventoryItemController inventoryItemController;
    public SlotController slotController;
    // Start is called before the first frame update
    void Start()
    {
        InventoryG = GameObject.Find("Canvas").transform.GetChild(0).gameObject;
    }
    
    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Inventory")){
            InventoryG.SetActive(!InventoryG.activeSelf);
        }
        float scrollDelta = Input.GetAxis("Mouse ScrollWheel");
        if (scrollDelta != 0f)
        {
            if(scrollDelta>0){
                if(inventoryManager.selectedSlot>=5){
                    inventoryManager.ChangeSelectedSlot(0);
                }
                else inventoryManager.ChangeSelectedSlot(inventoryManager.selectedSlot + 1);
            }
            else if(scrollDelta<0){
                if(inventoryManager.selectedSlot<=0){
                    inventoryManager.ChangeSelectedSlot(5);
                }
                else inventoryManager.ChangeSelectedSlot(inventoryManager.selectedSlot - 1);
            }
        }
        if(Input.GetButtonDown("Fire2")){
            if(slotController != null){
                if(slotController.transform.childCount == 0){
                    //GameObject  newHUDItem = Instantiate(inventoryItemController.gameObject);
                    inventoryManager.SpawnNewItem(inventoryItemController.item, slotController);
                    GameObject  newHUDItem = slotController.transform.GetChild(0).gameObject;
                    //GameObject  newHUDItem = Instantiate(inventoryItemController.gameObject);
                    newHUDItem.transform.SetParent(slotController.transform);
                    newHUDItem.GetComponent<InventoryItemController>().parentAfterDrag = slotController.transform;
                    newHUDItem.GetComponent<InventoryItemController>().count = 1;
                    newHUDItem.GetComponent<InventoryItemController>().RefreshCount();
                    inventoryItemController.count--;
                    inventoryItemController.RefreshCount();
                }
                else if(slotController.transform.GetChild(0).GetComponent<InventoryItemController>().item.name == inventoryItemController.item.name && inventoryItemController!=null){
                    slotController.transform.GetChild(0).GetComponent<InventoryItemController>().count++;
                    slotController.transform.GetChild(0).GetComponent<InventoryItemController>().RefreshCount();
                    inventoryItemController.count--;
                    inventoryItemController.RefreshCount();
                }
                inventoryManager.GetSelectedItem();
            }
        }
    }
}
