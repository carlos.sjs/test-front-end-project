using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class InventoryItemController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public InventoryManager inventoryManager;

    [Header("UI")]
    public Image image;
    public TextMeshProUGUI countText;

    [HideInInspector] public int count = 1;
    [HideInInspector] public ItemController item;
    public Transform parentAfterDrag;
    //public Transform parentBeforeDrag;

    private void Start() {
        inventoryManager = FindObjectOfType<InventoryManager>();
    }

    public void InitaliseItem(ItemController newItem){
        item = newItem;
        image.sprite = newItem.image;
        RefreshCount();
        //parentBeforeDrag = transform.parent;
    }
    
    public void RefreshCount(){
        countText.text = count.ToString();
        bool textActive = count > 1;
        countText.gameObject.SetActive(textActive);
        if(count <= 0) Destroy(this.gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData){
        if(GameObject.Find("Canvas").transform.GetChild(0).gameObject.activeSelf){
            image.raycastTarget = false;
            parentAfterDrag = transform.parent;
            transform.SetParent(transform.root);
            inventoryManager.GetSelectedItem();
        }
    }

    public void OnDrag(PointerEventData eventData){
        if(GameObject.Find("Canvas").transform.GetChild(0).gameObject.activeSelf){
            transform.position = Input.mousePosition;
            //Debug.Log(transform.parent.name);
        }
    }

    public void OnEndDrag(PointerEventData eventData){
        if(GameObject.Find("Canvas").transform.GetChild(0).gameObject.activeSelf){
            image.raycastTarget = true;
            transform.SetParent(parentAfterDrag);
            //parentBeforeDrag = parentAfterDrag;
            inventoryManager.GetSelectedItem();
        }
    }
}
