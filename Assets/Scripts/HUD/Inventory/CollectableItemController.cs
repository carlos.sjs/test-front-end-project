using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableItemController : MonoBehaviour
{
    public InventoryManager inventoryManager;
    public ItemController Item;
    private void Start() {
        inventoryManager = GameObject.Find("Game Controller").gameObject.GetComponent<InventoryManager>();
    }

    public void PickupItem(){
        bool result = inventoryManager.AddItem(Item);
        if(result)  Destroy(gameObject.transform.parent.gameObject);
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("Player")) PickupItem();
    }
}
