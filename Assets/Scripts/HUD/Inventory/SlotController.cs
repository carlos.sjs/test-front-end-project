using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SlotController : MonoBehaviour, IDropHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Image image;
    public Color selectedColor, notSelectedColor;
    HUDController hudController;

    private void Awake() {
        Deselect();
        hudController = FindObjectOfType<HUDController>();
    }

    public void Select(){
        image.color = selectedColor;
    }
   
    public void Deselect(){
        image.color = notSelectedColor;
    }

    public void OnDrop(PointerEventData eventData){
        if(GameObject.Find("Canvas").transform.GetChild(0).gameObject.activeSelf){
            if(transform.childCount == 0){
                InventoryItemController inventoryItemController = eventData.pointerDrag.GetComponent<InventoryItemController>();
                inventoryItemController.parentAfterDrag = transform;
            }
            else if(transform.GetChild(0).GetComponent<InventoryItemController>().item.name == eventData.pointerDrag.GetComponent<InventoryItemController>().item.name &&
                    transform.GetChild(0).GetComponent<InventoryItemController>().count < transform.GetChild(0).GetComponent<InventoryItemController>().inventoryManager.maxStacked &&
                    transform.GetChild(0).GetComponent<InventoryItemController>().item.stackable == true){

                if((transform.GetChild(0).GetComponent<InventoryItemController>().count + eventData.pointerDrag.GetComponent<InventoryItemController>().count) <= transform.GetChild(0).GetComponent<InventoryItemController>().inventoryManager.maxStacked){
                    transform.GetChild(0).GetComponent<InventoryItemController>().count += eventData.pointerDrag.GetComponent<InventoryItemController>().count;
                    transform.GetChild(0).GetComponent<InventoryItemController>().RefreshCount();
                    Destroy(eventData.pointerDrag.gameObject);
                }
                else{
                    int count = transform.GetChild(0).GetComponent<InventoryItemController>().inventoryManager.maxStacked - transform.GetChild(0).GetComponent<InventoryItemController>().count;
                    transform.GetChild(0).GetComponent<InventoryItemController>().count = transform.GetChild(0).GetComponent<InventoryItemController>().inventoryManager.maxStacked;
                    eventData.pointerDrag.GetComponent<InventoryItemController>().count -= count;
                    transform.GetChild(0).GetComponent<InventoryItemController>().RefreshCount();
                    eventData.pointerDrag.GetComponent<InventoryItemController>().RefreshCount();
                }
            }
            else if(transform.GetChild(0).GetComponent<InventoryItemController>().item.name != eventData.pointerDrag.GetComponent<InventoryItemController>().item.name ){
                InventoryItemController inventoryItemController = eventData.pointerDrag.GetComponent<InventoryItemController>();
                transform.GetChild(0).GetComponent<InventoryItemController>().parentAfterDrag = inventoryItemController.parentAfterDrag;
                transform.GetChild(0).transform.SetParent(inventoryItemController.parentAfterDrag);
                eventData.pointerDrag.gameObject.transform.SetParent(transform);
                inventoryItemController.parentAfterDrag = transform;
            }
            hudController.inventoryManager.GetSelectedItem();
        }
    }

    public void OnPointerDown(PointerEventData eventData){
        if(transform.parent.name == "Hotbar"){
            InventoryManager inventoryManager = FindObjectOfType<InventoryManager>();
            inventoryManager.ChangeSelectedSlot(transform.GetSiblingIndex());
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(eventData.pointerDrag != null){
            hudController.slotController = this;
            hudController.inventoryItemController = eventData.pointerDrag.GetComponent<InventoryItemController>();
            //Debug.Log(eventData.pointerDrag.name);
        }
    }

    // Implementación del método de la interfaz IPointerExitHandler
    public void OnPointerExit(PointerEventData eventData)
    {
        if(eventData.pointerDrag != null){
            hudController.slotController = null;
            hudController.inventoryItemController = null;
            //Debug.Log("Puntero fuera del objeto.");
        }
    }

}
