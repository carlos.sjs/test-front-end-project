using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Scriptable object/Item")]
public class ItemController : ScriptableObject
{
    [Header("Only Gameplay")]
    public ItemType type;
    //public string name;
    public ActionType actionType;
    public Vector2Int range = new Vector2Int(5,4);
    public GameObject ObjectModel;
    public Vector3 Offset;

    [Header("Only UI")]
    public bool stackable = false;

    [Header("Both")]
    public Sprite image;

}

public enum ItemType{
    Resource,
    Tool,
    Wepon
}

public enum ActionType{

}

