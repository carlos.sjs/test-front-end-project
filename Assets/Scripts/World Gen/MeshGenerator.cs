using UnityEngine;
using System.Collections;

public static class MeshGenerator{
    public static MeshData generate_terrain_mesh(float[,] height_map, float height_multiplier, AnimationCurve height_curve){
        int width = height_map.GetLength(0);
        int height = height_map.GetLength(1);
        float top_left_x = (width-1)/-2f;
        float top_left_z = (height-1)/2f;

        float threshold = 6.3f;
        threshold = 0;

        //Debug.Log("Top Left: " + top_left_x  + ", " + top_left_z);
        //Debug.Log("Bottom right" + (top_left_x+width-1)  + ", " + (top_left_z+height-1));


        MeshData mesh_data = new MeshData(width, height);
        int vertex_index = 0;


        float max_point = 0f;
        int invalid = 0, semivalid = 0, valid = 0;
        for(int y=0; y<height; y++)
            for(int x=0; x<width; x++){
                mesh_data.vertices[vertex_index] = new Vector3(top_left_x+x, height_curve.Evaluate(height_map[x,y])*height_multiplier, top_left_z-y);
                max_point = Mathf.Max(max_point, height_curve.Evaluate(height_map[x,y])*height_multiplier);

                mesh_data.uvs[vertex_index] = new Vector2(x/(float)width, y/(float)height);

                //Debug.Log(x  + ", " + y);
                //Debug.Log((top_left_x+x)  + ", " + (top_left_z-y));

                //if(x == 0 && y == 0)  mesh_data.vertices[vertex_index] = new Vector3(top_left_x+x, 100, top_left_z-y);

                if(mesh_data.vertices[vertex_index].y < threshold) mesh_data.g_vertex_count--;

                if(y<height-1 && x < width-1){
                    mesh_data.addTriangle(vertex_index, vertex_index+width+1, vertex_index+width);
                    mesh_data.addTriangle(vertex_index+width+1, vertex_index, vertex_index+1);
                }

                vertex_index++;
            }

        vertex_index = 0;
        for(int y=0; y<height; y++)
            for(int x=0; x<width; x++){
                if(y<height-1 && x < width-1){
                    int edge_inv = 0;
                    int other_inv = 0;
                    if(mesh_data.vertices[vertex_index].y < threshold) edge_inv++;
                    if(mesh_data.vertices[vertex_index + width + 1].y < threshold) edge_inv++;
                    if(mesh_data.vertices[vertex_index + 1].y < threshold) other_inv++;
                    if(mesh_data.vertices[vertex_index + width].y < threshold) other_inv++;

                    if(edge_inv == 1 && other_inv == 0){
                        mesh_data.g_triangle_count -= 3;
                        semivalid++;
                    }else if(edge_inv == 0 && other_inv == 1){
                        mesh_data.g_triangle_count -= 3;
                        semivalid++;
                    }else if(edge_inv+other_inv > 1){
                        mesh_data.g_triangle_count -= 6;
                        invalid++;
                    }else valid++;
                }

                vertex_index++;
            }

        vertex_index = 0;
        mesh_data.initGrassMesh();
        Debug.Log("Invalid triangle count: " + invalid);
        Debug.Log("Semi-valid triangle count: " + semivalid);
        Debug.Log("Valid triangle count: " + valid);

        for(int y=0; y<height; y++)
            for(int x=0; x<width; x++){
                if(y<height-1 && x < width-1){
                    int edge_inv = 0;
                    int other_inv = 0;
                    if(mesh_data.vertices[vertex_index].y < threshold) edge_inv++;
                    if(mesh_data.vertices[vertex_index + width + 1].y < threshold) edge_inv++;
                    if(mesh_data.vertices[vertex_index + 1].y < threshold) other_inv++;
                    if(mesh_data.vertices[vertex_index + width].y < threshold) other_inv++;

                    if(edge_inv == 1 && other_inv == 0){
                        if(mesh_data.vertices[vertex_index].y < threshold)
                            mesh_data.addGTriangle(vertex_index + 1, vertex_index+width+1, vertex_index+width);
                        else
                            mesh_data.addGTriangle(vertex_index, vertex_index+1, vertex_index+width);
                    }else if(edge_inv == 0 && other_inv == 1){
                        if(mesh_data.vertices[vertex_index + 1].y < threshold)
                            mesh_data.addGTriangle(vertex_index, vertex_index+width+1, vertex_index+width);
                        else
                            mesh_data.addGTriangle(vertex_index+width+1, vertex_index, vertex_index+1);
                    }else if(edge_inv+other_inv == 0){
                        mesh_data.addGTriangle(vertex_index, vertex_index+width+1, vertex_index+width);
                        mesh_data.addGTriangle(vertex_index+width+1, vertex_index, vertex_index+1);
                    }
                }
                vertex_index++;
            }


        //Debug.Log(max_point);
        return mesh_data;
    }
}

public class MeshData{
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;

    int triangle_index;
    int g_triangle_index;

    public Vector3[] grass_veritices;
    public int[] grass_triangles;
    public int g_vertex_count;
    public int g_triangle_count;

    public MeshData(int mesh_width, int mesh_height){
        Debug.Log("Creating mesh with " + (mesh_height*mesh_width) + " Veritices");
        vertices = new Vector3[mesh_height*mesh_width];
        uvs = new Vector2[mesh_width*mesh_height];
        triangles = new int[(mesh_height-1)*(mesh_width-1)*6];

        g_vertex_count = mesh_height*mesh_width;
        g_triangle_count = (mesh_height-1)*(mesh_width-1)*6;
        Debug.Log(g_triangle_count);
    }

    public void initGrassMesh(){
        Debug.Log(g_triangle_count);
        grass_triangles = new int[g_triangle_count];
    }

    public void addTriangle(int a, int b, int c){
        triangles[triangle_index] = a;
        triangles[triangle_index+1] = b;
        triangles[triangle_index+2] = c;
        triangle_index += 3;
    }

    public void addGTriangle(int a, int b, int c){
        grass_triangles[g_triangle_index] = a;
        grass_triangles[g_triangle_index+1] = b;
        grass_triangles[g_triangle_index+2] = c;
        g_triangle_index += 3;
    }

    public Mesh createMesh(){
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        return mesh;
    }

    public Mesh createGMesh(){
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = grass_triangles;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
        return mesh;
    }

}