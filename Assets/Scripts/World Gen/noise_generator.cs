using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class noise_generator{
    public static float[,] generateNoiseMap(int width, int height, float scale, int octaves, float persistance, float lacunarity, int seed){
        float[,] noise_map = new float[width, height];

        float maxNH = float.MinValue, minNH = float.MaxValue;
    
        Vector2[] octaves_offset = new Vector2[octaves];
        System.Random rnd = new System.Random(seed);
        for(int i=0; i<octaves; i++){
            octaves_offset[i] = new Vector2(rnd.Next(-100000, 100000), rnd.Next(-100000, 100000));
        }
        
        Vector2 hscale = new Vector2(width/2f, height/2f);

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                float amplitude = 1;
                float frequency = 1;
                float noise_height = 0;

                for(int i=0; i<octaves; i++){
                    float sampleX = (x - hscale.x) / scale * frequency;
                    float sampleY = (y - hscale.y) / scale * frequency;

                    float perlinval = Mathf.PerlinNoise(sampleX + octaves_offset[i].x, sampleY + octaves_offset[i].y) * 2 - 1;

                    noise_height += perlinval * amplitude;
                    frequency *= persistance;
                    frequency *= lacunarity;
                }

                if(noise_height < minNH) minNH = noise_height;
                else if(noise_height > maxNH) maxNH = noise_height;

                noise_map[x, y] = noise_height;
            }
        }

        for(int x = 0; x<width; x++)
            for(int y=0; y<height; y++) noise_map[x, y] = Mathf.InverseLerp(minNH, maxNH, noise_map[x, y]);

        return noise_map;
    }
}
