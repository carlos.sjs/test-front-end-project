using UnityEngine;

public static class TextureGenerator_{
    public static Texture2D  get_texture_map(float[,] noise_map, float[,] temp_map, float[,] humidity_map, MapRenderer.TerrainType[] terrains, MapRenderer.Biome[] biomes, int[,] biomemap){
        int width = noise_map.GetLength(0);
        int height = noise_map.GetLength(1);

        Texture2D texture = new Texture2D(width, height);

        Color[] terrain_map = new Color[width*height];
        Color[] biome_map = getBiomeColorMap(temp_map, humidity_map, biomes, biomemap);

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                for(int i = 0; i < terrains.Length; i++){
                    if(x == 0 && y == 0){
                        terrain_map[x+y*width] = new Color(1f, 0f, 0f, 1f);
                        break;
                    }

                    if(x == width-1 && y == height-1){
                        terrain_map[x+y*width] = new Color(1f, 0f, 0f, 1f);
                        break;
                    }

                    if(terrains[i].height >= noise_map[x,y]){
                        terrain_map[x+y*width] = terrains[i].color;
                        break;
                    }
                    if(terrains[i].name == "Sand"){
                        terrain_map[x+y*width] = biome_map[x+y*width];
                        break;
                    }
                }
            }

        texture.SetPixels(terrain_map);
        texture.Apply();

        return texture;
    }

    private static Color[] getBiomeColorMap(float[,] temp_map, float[,] hum_map, MapRenderer.Biome[] biomes, int[,] biomemap){
        int width = temp_map.GetLength(0);
        int height = temp_map.GetLength(1);

        Color[] biome_map = new Color[width*height];

        /*for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                biome_map[x+y*width] = biomes[biomemap[(int)(temp_map[x,y]/0.2f), (int)(hum_map[x,y]/0.2f)]].color;
            }*/

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                biome_map[x+y*width] = biomes[4].color;
            }
        
        return biome_map;
    }

    public static float[,] applyFallOff(float[,] noise_map, float[,] falloff_map){
        int width = noise_map.GetLength(0);
        int height = noise_map.GetLength(1);
        for(int y=0; y<height; y++)
            for(int x=0; x<width; x++)
                noise_map[x, y] = Mathf.Max(noise_map[x, y]-falloff_map[x, y], 0f);
        
        return noise_map;
    }

    public static Texture2D getHeightMap(float minHeight, float minFullHeight, float maxHeight, float[,] noise_map, AnimationCurve height_curve){
        int width = noise_map.GetLength(1);
        int height = noise_map.GetLength(0);
        Texture2D heightMap = new Texture2D(width, height, TextureFormat.RGBAFloat, true, true);
        heightMap.filterMode = FilterMode.Point;
        heightMap.wrapMode = TextureWrapMode.Clamp;
        heightMap.anisoLevel = 1;
        Color[] colorMap = new Color[width*height];

        for(int y = 0; y<height; y++)
            for(int x=0; x<width; x++){
                float gmValue = height_curve.Evaluate(noise_map[x, (height-1)-y]);
                if(gmValue < minHeight || gmValue > maxHeight) gmValue = 0f;
                else if(gmValue >= minFullHeight) gmValue = 1f;
                else gmValue = 1 - (minFullHeight - gmValue) / (minFullHeight-minHeight);
                colorMap[x + y*width] = new Color(gmValue, gmValue, gmValue);
            }
        
        heightMap.SetPixels(colorMap);
        heightMap.Apply();
        return heightMap;
    }
}