Shader "Custom/TerrainColor"{
    Properties{
        
    }
    SubShader{
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        const static int max_layer_count = 10;
        const static float eps = 1E-12;

        float min_height;
        float max_height;

        int layer_count;
        float3 tint_colors[max_layer_count];
        float tint_strenght[max_layer_count];
        float start_heights[max_layer_count];
        float texture_scales[max_layer_count];
        float blends[max_layer_count];

        UNITY_DECLARE_TEX2DARRAY(textures);

        struct Input{
            float3 worldPos;
            float3 worldNormal;
        };

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float inverse_lerp(float a, float b, float v){
            return saturate((v-a)/ (b-a));
        }

        float3 triplanar(float3 worldPos, float scale, float3 blendAxes, int textureIndex) {
			float3 scaledWorldPos = worldPos / scale;
			float3 xProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(scaledWorldPos.y, scaledWorldPos.z, textureIndex)) * blendAxes.x;
			float3 yProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(scaledWorldPos.x, scaledWorldPos.z, textureIndex)) * blendAxes.y;
			float3 zProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(scaledWorldPos.x, scaledWorldPos.y, textureIndex)) * blendAxes.z;
			return xProjection + yProjection + zProjection;
		}

        void surf (Input IN, inout SurfaceOutputStandard o){
            float height_p = inverse_lerp(min_height, max_height, IN.worldPos.y);
            //o.Albedo = height_p; 
            float3 blend_axes = abs(IN.worldNormal);
			blend_axes /= blend_axes.x + blend_axes.y + blend_axes.z;

            for(int i=0; i<layer_count; i++){
                float draw_strenght = inverse_lerp(-blends[i]/2 - eps, blends[i]/2, height_p - start_heights[i]);
                
                float3 base_color = tint_colors[i] * tint_strenght[i];
				float3 texture_color = triplanar(IN.worldPos, texture_scales[i], blend_axes, i) * (1-tint_strenght[i]);

				o.Albedo = o.Albedo * (1-draw_strenght) + (base_color+texture_color) * draw_strenght;
            }
        }
        ENDCG
    }
    FallBack "Diffuse"
}
