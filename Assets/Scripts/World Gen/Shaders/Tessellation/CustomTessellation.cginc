// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Tessellation programs based on this article by Catlike Coding:
// https://catlikecoding.com/unity/tutorials/advanced-rendering/tessellation/

struct vertexInput{
	float4 vertex : POSITION;
	float3 normal : NORMAL;
	float4 tangent : TANGENT;
	float2 uv      : TEXCOORD0;
};

struct vertexOutput{
	float4 vertex : SV_POSITION;
	float3 normal : NORMAL;
	float4 tangent : TANGENT;
	float2 uv      : TEXCOORD0;
};

struct TessellationFactors {
	float edge[3] : SV_TessFactor;
	float inside : SV_InsideTessFactor;
};

vertexInput vert(vertexInput v){
	return v;
}

vertexOutput tessVert(vertexInput v){
	vertexOutput o;
	// Note that the vertex is NOT transformed to clip
	// space here; this is done in the grass geometry shader.
	o.vertex = v.vertex;
	o.normal = v.normal;
	o.tangent = v.tangent;
	return o;
}

float _TessellationUniform;
float _MaxCameraDistance;

bool in_frustum(float4 p) {
            float4 Pclip = UnityObjectToClipPos(float4(p.x, p.y, p.z, 1.0));
            return abs(Pclip.x) < Pclip.w &&
                abs(Pclip.y) < Pclip.w &&
                0 < Pclip.z &&
                Pclip.z < Pclip.w;
        }

TessellationFactors patchConstantFunction (InputPatch<vertexInput, 3> patch){
	TessellationFactors f;
	if(in_frustum(patch[0].vertex) || in_frustum(patch[1].vertex) || in_frustum(patch[2].vertex) || distance(_WorldSpaceCameraPos, mul(unity_ObjectToWorld, patch[0].vertex)) < _MaxCameraDistance/10.0){
		int tfactor = ceil(lerp(_TessellationUniform, 1, saturate(distance(_WorldSpaceCameraPos, mul(unity_ObjectToWorld, patch[0].vertex)) / _MaxCameraDistance)));
		f.edge[0] = tfactor;
		f.edge[1] = tfactor;
		f.edge[2] = tfactor;
		f.inside = tfactor;
	}else{
		f.edge[0] = 1;
		f.edge[1] = 1;
		f.edge[2] = 1;
		f.inside = 1;
	}
	return f;
}

[UNITY_domain("tri")]
[UNITY_outputcontrolpoints(3)]
[UNITY_outputtopology("triangle_cw")]
[UNITY_partitioning("integer")]
[UNITY_patchconstantfunc("patchConstantFunction")]
vertexInput hull (InputPatch<vertexInput, 3> patch, uint id : SV_OutputControlPointID){
	return patch[id];
}

[UNITY_domain("tri")]
vertexOutput domain(TessellationFactors factors, OutputPatch<vertexInput, 3> patch, float3 barycentricCoordinates : SV_DomainLocation){
	vertexInput v;

	#define MY_DOMAIN_PROGRAM_INTERPOLATE(fieldName) v.fieldName = \
		patch[0].fieldName * barycentricCoordinates.x + \
		patch[1].fieldName * barycentricCoordinates.y + \
		patch[2].fieldName * barycentricCoordinates.z;

	MY_DOMAIN_PROGRAM_INTERPOLATE(vertex)
	MY_DOMAIN_PROGRAM_INTERPOLATE(normal)
	MY_DOMAIN_PROGRAM_INTERPOLATE(tangent)
	MY_DOMAIN_PROGRAM_INTERPOLATE(uv)

	return tessVert(v);
}