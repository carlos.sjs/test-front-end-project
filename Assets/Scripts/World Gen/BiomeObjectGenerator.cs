using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BiomeObjectGenerator{

    public static void ForestGenerator(BiomeData biome_data, float[,] noise_map, float unifform_scale, GameObject terrain_mesh, MeshData mesh_data, ref Texture2D grassMap){
        int map_width = noise_map.GetLength(0);
        int map_height = noise_map.GetLength(1);
        if(biome_data.use_trees){
            List<Vector2> valid_spawns = new List<Vector2>();
            List<Vector2> spawned_trees = new List<Vector2>();
            int tree_amount = Random.Range(biome_data.min_amount_trees, biome_data.max_amount_trees + 1);
            int starting_amount = Mathf.Max((int)(tree_amount*.1f), 1);

            for(int x=0; x<map_width; x++)
                for(int y=0; y<map_height; y++){
                    
                    if(noise_map[x,y] > biome_data.tree_treshold){
                        valid_spawns.Add(new Vector2(x, y));
                        //valid_spawns.Add(mesh_data.vertices[y*map_height + x]*unifform_scale);
                    }
                }

            for(int i=0; i<Mathf.Min(valid_spawns.Count, starting_amount); i++){
                int random_index = Random.Range(0, valid_spawns.Count);
                Vector2 spawn_position = valid_spawns[random_index];
                valid_spawns.RemoveAt(random_index);
                spawned_trees.Add(spawn_position);

                GameObject generated_tree = GameObject.Instantiate(biome_data.trees[Random.Range(0, biome_data.trees.Length)], mesh_data.vertices[(int)spawn_position.y*map_height + (int)spawn_position.x]*unifform_scale, Quaternion.identity);
                generated_tree.transform.SetParent(terrain_mesh.transform);
                generated_tree.transform.localScale = new Vector3(3/unifform_scale, 3/unifform_scale, 3/unifform_scale);

                grassMap.SetPixel((int)spawn_position.x, (map_height-1)-(int)spawn_position.y, new Color(.6f, .6f, .6f));
            }

            int created_count = starting_amount;
            int[] dx = {1, 1,  1, 0,  0, -1, -1, -1};
            int[] dy = {0, 1, -1, 1, -1,  0, 1,  -1};
            while(created_count < tree_amount){
                int dir = Random.Range(0,8);
                Vector2 orig = spawned_trees[Random.Range(0, spawned_trees.Count)];
                orig += new Vector2(dx[dir], dy[dir]) * Random.Range(2, 4);

                if(orig.y < 0 || orig.x < 0 || orig.y >= map_height || orig.x >= map_width) continue;

                if(noise_map[(int)orig.x, (int)orig.y] > biome_data.tree_treshold && !spawned_trees.Contains(orig)){
                    spawned_trees.Add(orig);

                    GameObject generated_tree = GameObject.Instantiate(biome_data.trees[Random.Range(0, biome_data.trees.Length)], mesh_data.vertices[(int)orig.y*map_height + (int)orig.x]*unifform_scale, Quaternion.identity);
                    generated_tree.transform.SetParent(terrain_mesh.transform);
                    generated_tree.transform.localScale = new Vector3(3/unifform_scale, 3/unifform_scale, 3/unifform_scale);

                    grassMap.SetPixel((int)orig.x, (map_height-1)-(int)orig.y, Color.black);

                    created_count++;
                }
            }
        }
        grassMap.Apply();
    }
}
