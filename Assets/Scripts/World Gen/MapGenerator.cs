using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour{
    public enum RenderMode{
        noise_map,
        falloff_map,
        falloff_and_noise,
        terrain_map,
        temperature_map,
        humidity_map,
        biome_map,
        biome_and_terrain,
        mesh
    };

    [Header("Rendering Settings")]
    public RenderMode render_mode;

    public bool autoUpdate;

    [Header("Data")]
    public NoiseData noise_data;
    public TerrainData terrain_data;
    public TextureData texture_data;
    public BiomeData biome_data;
    public Material terrainMaterial;
    public GameObject terrain_mesh;

    [Header("Grass Data")]
    public Material grassMaterial;
    public float statringGrassHeight;
    public float minFullGrassHeight;
    public float maxFullGrassHeight;

    [Header("World Size")]
    public int width;
    public int height;

    

    public MapRenderer.TerrainType[] terrain_types;

    [Header("Biome Settings")]
    public MapRenderer.Biome[] biomes; 
    public int[,] biomeDistribution = {
        {2,2,5,7,8},
        {1,5,5,7,8},
        {1,5,4,7,7},
        {0,4,4,4,4},
        {0,0,3,6,6}
    };

    [Header("Temperature")]
    public float temperature_noise_scale;
    public int temperature_octaves;
    public float temperature_persistance;
    public float temperature_lacunarity;

    [Header("Humidity")]
    public float humidity_noise_scale;
    public int humidity_octaves;
    public float humidity_persistance;
    public float humidity_lacunarity;

    [Header("Rendering Shit")]
    public MapRenderer map_renderer;
     
    private float[,] falloff_map;

    void Start(){
        Debug.Log("We should gen map now");
        generateMap(true);
    }

    void OnValuesUpdated(){
        if(!Application.isPlaying){
            generateMap(false);
        }
    }

    void OnTextureValuesUpdated(){
        texture_data.ApplyToMaterial(terrainMaterial);
    }

    public void generateMap(bool is_in_runtime){
        Random.InitState(noise_data.seed);

        float[,] noise_map = noise_generator.generateNoiseMap(width, height, noise_data.noise_scale, noise_data.octaves, noise_data.persistance, noise_data.lacunarity, noise_data.seed);
        //float[,] noise_map = terrain_geneartor.generator_1(width, height);
        noise_map = terrain_geneartor.generator_3(width, height, noise_map);
        //float[,] noise_map = terrain_geneartor.noise_distribution(width, height);
        
        float[,] temperature_noise = noise_generator.generateNoiseMap(width, height, temperature_noise_scale, temperature_octaves, temperature_persistance, temperature_lacunarity, 69*noise_data.seed);
        float[,] humidity_noise = noise_generator.generateNoiseMap(width, height, humidity_noise_scale, humidity_octaves, humidity_persistance, humidity_lacunarity, 25*noise_data.seed);
        int[,] biome_map = generateBiomeMap(temperature_noise, humidity_noise);

        if(render_mode == RenderMode.noise_map) map_renderer.renderRawMap(noise_map);
        else if(render_mode == RenderMode.terrain_map) map_renderer.renderTerrainMap(noise_map, falloff_map, terrain_data.use_falloff_map, terrain_types);
        else if(render_mode == RenderMode.falloff_map) map_renderer.renderRawMap(falloff_map);
        else if(render_mode == RenderMode.falloff_and_noise) map_renderer.renderFalloffNoise(noise_map, falloff_map);
        else if(render_mode == RenderMode.temperature_map) map_renderer.renderTempMap(temperature_noise);
        else if(render_mode == RenderMode.humidity_map) map_renderer.renderHumidityMap(humidity_noise);
        else if(render_mode == RenderMode.biome_map) map_renderer.renderBiomeMap(temperature_noise, humidity_noise, biomes, biomeDistribution);
        else if(render_mode == RenderMode.biome_and_terrain) map_renderer.renderBiomeTerrainMap(noise_map, temperature_noise, humidity_noise, falloff_map, terrain_data.use_falloff_map, terrain_types, biomes, biomeDistribution);
        else if(render_mode == RenderMode.mesh){
            if(terrain_data.use_falloff_map)
                noise_map = TextureGenerator_.applyFallOff(noise_map, falloff_map);
            MeshData mesh_data = MeshGenerator.generate_terrain_mesh(noise_map, terrain_data.height_multiplier, terrain_data.height_curve);

            Texture2D grassMap = TextureGenerator_.getHeightMap(statringGrassHeight, minFullGrassHeight, maxFullGrassHeight, noise_map, terrain_data.height_curve);
            if(is_in_runtime) BiomeObjectGenerator.ForestGenerator(biome_data, noise_map, terrain_data.uniform_scale, terrain_mesh, mesh_data, ref grassMap);
            map_renderer.renderMeshBiomeMap(mesh_data, TextureGenerator_.get_texture_map(noise_map, temperature_noise, humidity_noise, terrain_types, biomes, biomeDistribution), is_in_runtime);
            
            grassMaterial.SetTexture("_GrassMap", grassMap);
            grassMaterial.SetInteger("_MeshWidth", noise_map.GetLength(0));
            grassMaterial.SetInteger("_MeshHeight", noise_map.GetLength(1));
        }

        texture_data.UpdateMeshHeight(terrainMaterial, terrain_data.minHeight, terrain_data.maxHeight);
        Debug.Log(terrain_data.minHeight + ", " + terrain_data.maxHeight);
    }


    private int[,] generateBiomeMap(float[,] temperature_map, float[,] humidity_map){
        int[,] biome_map =  new int[width, height];

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                biome_map[x,y] = biomeDistribution[(int)(temperature_map[x,y]/0.2f), (int)(humidity_map[x,y]/0.2f)];
            }

        return biome_map;
    }

    void OnValidate(){
        if(terrain_data != null){
            terrain_data.OnValuesUpdated -= OnValuesUpdated;
            terrain_data.OnValuesUpdated += OnValuesUpdated;
        }   
        if(noise_data != null){
            noise_data.OnValuesUpdated -= OnValuesUpdated;
            noise_data.OnValuesUpdated += OnValuesUpdated;
        }
        if(texture_data != null){
            texture_data.OnValuesUpdated -= OnTextureValuesUpdated;
            texture_data.OnValuesUpdated += OnTextureValuesUpdated;
        }

        if(height < 1) height = 1;
        if(width < 1) width = 1;

        falloff_map = FalloffGenerator.getFalloffMap(width, height, 3f, 4f);
    }

}
