using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point{
    private (float, float) tuple;

    public Point(float item1, float item2){
        tuple = (item1, item2);
    }

    public Point((float, float) t){
        tuple = (t.Item1, t.Item2);
    }

    public float x => tuple.Item1;
    public float y => tuple.Item2;

    public static Point operator+ (Point a, Point b){
        return new Point(a.x+b.x, a.y+b.y);
    }
    public static Point operator- (Point a, Point b){
        return new Point(a.x-b.x, a.y-b.y);
    }
    public static float operator* (Point a, Point b){
        return a.x*b.y - a.y*b.x;
    }
    public static Point operator* (float c, Point a){
        return new Point(c*a.x, c*a.y);
    }

    public override string ToString(){
        return "(" + x + ", " + y + ") ";
    }
}

public static class terrain_geneartor{
    private static int cells = 10;

    public static float[,] generator_1(int width, int height, float[,] noise){
        float[,] terrain_map = new float[width, height];

        int initial_vertex_count = Random.Range(6, 9);
        int iteration_count = 4;

        float chance = .5f;
        float border = .1f*width;
        float spacing = .05f*width;



        int offset = Random.Range(0, 8);
        //offset = 0;
        int current_vertex_count = 0;

        int vertex_count = initial_vertex_count<<iteration_count;
        int jump = (1<<iteration_count), new_jump = (1<<(iteration_count-1));

        //Debug.Log("Jump: " + jump);
        //Debug.Log("Vertex count: " + vertex_count);

        Point[] vertex = new Point[vertex_count];
        Point[] external_reference = new Point[vertex_count];
        Point[] internal_reference = new Point[vertex_count];
        

        //width -= 10;
        //height -= 10;

        float topx = 0, topy = 0, downx = 0, downy = 0;
        (float, float)[] reference_points = {(border,border), (width/2f,border), ((width-border), border), ((width-border), height/2f), (width-border, height-border), (width/2f, height-border), (border, height-border), (border, height/2f)};
        float sumX = 0f, sumY = 0f;


        for(int i=0; i<8 && current_vertex_count < initial_vertex_count; i++){
            float rnd = Random.Range(0f,1f);
            if(current_vertex_count < initial_vertex_count && current_vertex_count + 7-i > initial_vertex_count && rnd <= chance){
                chance += .2f;
                continue;
            }

            if((i+offset)%8 == 0){topx = 0 + border; topy = 0+border; downx = width/3f-spacing; downy = height/3f-spacing;}
            else if((i+offset)%8 == 1){topx = width/3f+spacing; topy = 0+border; downx = width-width/3f-spacing; downy = height/3f-spacing;}
            else if((i+offset)%8 == 2){topx = width-width/3f+spacing; topy = 0+border; downx = width-border; downy = height/3f-spacing;}
            else if((i+offset)%8 == 3){topx = width-width/3f+spacing; topy = height/3+spacing; downx = width-border; downy = height-height/3f-spacing;}
            else if((i+offset)%8 == 4){topx = width-width/3f+spacing; topy = height-height/3+spacing; downx = width-border; downy = height-border;}
            else if((i+offset)%8 == 5){topx = width/3f+spacing; topy = height-height/3f+spacing; downx = width-width/3f-spacing; downy = height-border;}
            else if((i+offset)%8 == 6){topx = 0+border; topy = height-height/3f+spacing; downx = width/3f-spacing; downy = height-border;}            
            else if((i+offset)%8 == 7){topx = 0+border; topy = height/3f+spacing; downx = width/3f-spacing; downy = height-height/3f-spacing;}
            
            //Debug.Log(topx + ", " + topy + "   " + downx + ", " + downy);
            
            float newX = Random.Range(topx, downx), newY = Random.Range(topy, downy);
            vertex[current_vertex_count*jump] = new Point(newX, newY);
            sumX += newX;
            sumY += newY;
            //Debug.Log("The cuadrant index is " + (i+offset)%8 + ", (" + vertex[current_vertex_count].Item1 + ", " + vertex[current_vertex_count].Item2 + ")");

            external_reference[current_vertex_count*jump] = new Point(reference_points[(i+offset)%8]);

            current_vertex_count++;
        }

        sumX /= current_vertex_count;
        sumY /= current_vertex_count;
        for(int i=0; i<initial_vertex_count; i++){
            internal_reference[i*jump] = new Point(sumX, sumY);
        }

        //Debug.Log(current_vertex_count + " " + (current_vertex_count == initial_vertex_count));
        
        // Recursively do some shit or something
        for(int i=1; i<=iteration_count; i++){

            //Debug.Log("Jump: " + jump + ", next: " + new_jump);
            
            for(int j = 0; j < vertex_count; j+=jump){
                int next = (j+jump)%vertex_count;

                //Debug.Log("----------------------------------------");
                //Debug.Log(j + ": " + vertex[j]);
                //Debug.Log(next + ": " + vertex[next]);
                //Debug.Log("----------------------------------------");

                Point inner_c = new Point(
                    (vertex[j].x + internal_reference[j].x + vertex[next].x + internal_reference[next].x) / 4f,
                    (vertex[j].y + internal_reference[j].y + vertex[next].y + internal_reference[next].y) / 4f
                );

                Point outer_c = new Point(
                    (vertex[j].x + external_reference[j].x + vertex[next].x + external_reference[next].x) / 4f,
                    (vertex[j].y + external_reference[j].y + vertex[next].y + external_reference[next].y) / 4f
                );

                //Debug.Log("Quad - " + outer_c + vertex[j] + vertex[next] + inner_c);

                vertex[j+new_jump] = random_in_quad(outer_c, vertex[j], vertex[next], inner_c);
                internal_reference[j+new_jump] = inner_c;
                external_reference[j+new_jump] = outer_c;
            }

            jump = new_jump;
            new_jump = 1<<(iteration_count-i-1);
        }

        //Debug.Log("Done!");

        // Fill island
        for(int y = 0; y<height; y++){
            for(int x=0; x<width; x++){
                int ct = 0;
                for(int i=0; i<vertex_count; i++){
                    if((float)y > Mathf.Min(vertex[i].y, vertex[(i+1)%vertex_count].y) && (float)y <= Mathf.Max(vertex[i].y, vertex[(i+1)%vertex_count].y)){
                        Point e1 = (vertex[i].y < vertex[(i+1)%vertex_count].y) ? vertex[i]:vertex[(i+1)%vertex_count];
                        Point e2 = (vertex[i].y >= vertex[(i+1)%vertex_count].y) ? vertex[i]:vertex[(i+1)%vertex_count];
                        
                        Point p1 = e2-e1;
                        Point p2 = new Point((x)-e1.x, (y)-e1.y);
                        float cross = p1*p2;

                        if(cross > 0) ct++;
                    }
                }

                if(ct%2==1) terrain_map[x,y] = 1f;
                else terrain_map[x,y] = 0f;
            }
        }

        /*for(int y = 0; y<height; y++)
            for(int x=0; x<width; x++)
                for(int i=0; i<vertex_count; i++)
                    if((x-vertex[i].x)*(x-vertex[i].x) + (y-vertex[i].y)*(y-vertex[i].y) <= 1f)
                        terrain_map[x,y] = (float)(i+1)/vertex_count;*/


        return terrain_map;
    }

    public static float[,] generator_2(int width, int height, float[,] noise){
        float[,] base_map = generator_1(width, height, noise);

        int blur_passes = 4;
        for(int i=0; i<blur_passes; i++)
            base_map = blur_pass(base_map);

        for(int x=0; x<width; x++)
            for(int y=0; y<height; y++)
                base_map[x, y] = base_map[x,y] * (noise[x, y]/2f + .5f);

        return base_map;
    }

    public static float[,] generator_3(int width, int height, float[,] noise){
        float[,] base_map = generator_1(width, height, noise);
        float[,] bitch_map = new float[width,height];
        int[,] dist = new int[width,height];

        int[] dx = {1, 0, -1, 0};
        int[] dy = {0, -1, 0, 1};

        Queue<Point> q = new Queue<Point>();
        for(int x=0; x<width; x++)
            for(int y=0; y<height; y++){
                if(base_map[x,y]==0f) continue;
                bool is_edge = false;
                for(int i=0; i<4; i++){
                    int nx = x+dx[i], ny = y+dy[i];
                    if(nx >= 0  && ny >= 0 && nx < width && ny < height){
                        if(base_map[nx,ny] == 0f){
                            is_edge = true;

                            break;
                        }
                    }
                }

                if(is_edge){
                    base_map[x,y] = .49f;
                    dist[x,y] = 0;
                    q.Enqueue(new Point((float)x, (float)y));
                }
            }
                

        while(q.Count > 0){
            var nd = q.Dequeue();
            if(dist[(int)nd.x,(int)nd.y] > 6)
                continue;

            for(int i=0; i<4; i++){
                Point nx = nd + new Point(dx[i], dy[i]);
                if(nx.x >= 0  && nx.y >= 0 && nx.x < width && nx.y < height)
                if(bitch_map[(int)nx.x,(int)nx.y] != .49f){
                    bitch_map[(int)nx.x,(int)nx.y] = .49f;
                    dist[(int)nx.x,(int)nx.y] = dist[(int)nd.x,(int)nd.y]+1;
                    q.Enqueue(nx);
                }
            }
        }

        for(int x=0; x<width; x++)
            for(int y=0; y<height; y++){
                if(bitch_map[x,y] == 0f)
                    base_map[x, y] = base_map[x,y] * (noise[x, y]/2f + .5f);
                else
                    base_map[x, y] = bitch_map[x, y];
                //base_map[x,y] = (noise[x, y]/2f + .5f);
            }
        
        int blur_passes = 1;
        for(int i=0; i<blur_passes; i++)
            base_map = blur_pass(base_map);

        return base_map;
    }

    // a = c1, b&c quad points, d = c2
    private static Point random_in_quad(Point a, Point b, Point c, Point d){
        float a1 = Mathf.Abs(a.x*(b.y-c.y) + b.x*(c.y-a.y) + c.x*(a.y-b.y));
        float a2 = Mathf.Abs(d.x*(b.y-c.y) + b.x*(c.y-d.y) + c.x*(d.y-b.y));

        float scale = .5f, offset = Random.Range(0f, 1000f);

        //float rnd = Mathf.Clamp01(Mathf.PerlinNoise((b.x-c.x)*scale + offset, (b.y-c.y)*scale + offset))*(a1+a2);
        float rnd = Random.Range(0f, a1+a2);
        if(rnd <= a1) return random_in_triangle(a, b, c);
        else return random_in_triangle(b,d,c);
    }

    private static Point random_in_triangle(Point a, Point b, Point c){
        //Debug.Log(a + ", " + b + ", " + c);

        float scale = .2f;
        float r1 = Mathf.Clamp01(Mathf.PerlinNoise(b.x/240f, b.y/240f)), r2 = Mathf.Clamp01(Mathf.PerlinNoise(b.x/240f, b.y/240f));

        Point x = r1*(a-c)+r2*(b-c);

        if(PointInTriangle(x+c, a, b, c)){
            //Debug.Log("IN: " + (x+c));
            return x+c;
        }
        else{
            //Debug.Log("OUT:" + " orig." + (x+c) + " " +(c+(a-c)+(b-c)-x));
            return c+(a-c)+(b-c)-x;
        }
    }

    public static bool PointInTriangle(Point p, Point p0, Point p1, Point p2){
        var s = (p0.x - p2.x) * (p.y - p2.y) - (p0.y - p2.y) * (p.x - p2.x);
        var t = (p1.x - p0.x) * (p.y - p0.y) - (p1.y - p0.y) * (p.x - p0.x);

        if ((s < 0) != (t < 0) && s != 0 && t != 0)
            return false;

        var d = (p2.x - p1.x) * (p.y - p1.y) - (p2.y - p1.y) * (p.x - p1.x);
        return d == 0 || (d < 0) == (s + t <= 0);
    }

    public static float[,] noise_distribution(int width, int height){
        int samples = 1000000;
        float point_value = 0.0001f;

        float[,] noise_d = new float[width, height];

        for(int i=0; i<samples; i++){
            float x = Random.Range(0f, 1f);
            float y = Random.Range(0f, 1f);

            float noise = Mathf.Clamp01(Mathf.PerlinNoise(x, y));
            noise *= (float)(width-1);

            for(int y_=0; y_<height; y_++)
                noise_d[(int) noise, y_] += point_value;
                
        }

        return noise_d;
    }

    public static float[,] blur_pass(float[,] map){
        int width = map.GetLength(0);
        int height = map.GetLength(1);

        float[,] new_map =  new float[width, height];

        int kernel_size = 12;

        for(int x = 0; x<width; x++)
            for(int y=0; y<height; y++){
                float sum = 0f;
                int ct = 0;
                for(int dx = -(kernel_size-1)/2; dx <= (kernel_size-1)/2; dx++)
                    for(int dy = -(kernel_size-1)/2; dy <= (kernel_size-1)/2; dy++)
                        if(x+dx >= 0 && x+dx < width && y+dy >= 0 && y+dy < height){
                            sum += map[x+dx, y+dy];
                            ct++;
                        }
                new_map[x, y] = sum/(float)ct;
            }
        
        return new_map;
    }
}