using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FalloffGenerator{

    public static float[,] getFalloffMap(int width, int height, float a, float b){
        

        float[,] map = new float[width, height];

        for(int x=0; x<width; x++){
            for(int y=0; y<height; y++){
                float max_dist = Mathf.Max(Mathf.Abs(x/(float)width*2-1), Mathf.Abs(y/(float)height*2-1));
                map[x,y] = evaluate(max_dist, a, b);
            }
        }

        return map;
    }
    

    public static float[,] getFalloffMap(int width, int height){
        return getFalloffMap(width, height, 3, 2.2f);
    }

    private static float evaluate(float x, float a, float b){
        return Mathf.Pow(x, a) / (Mathf.Pow(x, a) + Mathf.Pow(b - b*x, a));
    }
}
