using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;

public class MapRenderer: MonoBehaviour{
    public Renderer texture_renderer;

    public MeshFilter meshFilter;
    public MeshFilter grassMeshFilter;
    public MeshCollider meshCollider;
	public MeshRenderer meshRenderer;

    [SerializeField]
    private NavMeshSurface navMesh;

    public void renderRawMap(float[,] noise_map){
        int width = noise_map.GetLength(0); 
        int height = noise_map.GetLength(1);

        Color[] color_map = new Color[width*height];

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++)
                    color_map[y*width+x] = new Color(noise_map[x,y], noise_map[x,y], noise_map[x,y]);

        renderMap(color_map, width, height);
    }

    public void renderFalloffNoise(float[,] noise_map, float[,] falloff_map){
        int width = noise_map.GetLength(0);
        int height = noise_map.GetLength(1);

        Color[] raw_map = new Color[width*height];

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                float value = noise_map[x,y]-falloff_map[x,y];
                raw_map[x+y*width] = new Color(value, value, value);
            }

        renderMap(raw_map, width, height);
    }

    public void renderTerrainMap(float[,] noise_map, float[,] falloff_map, bool use_falloff, TerrainType[] terrains){
        int width = noise_map.GetLength(0);
        int height = noise_map.GetLength(1);

        Color[] terrain_map = new Color[width*height];

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                if(use_falloff) noise_map[x, y] -= falloff_map[x, y];

                for(int i = 0; i < terrains.Length; i++)
                    if(terrains[i].height >= noise_map[x,y]){
                        terrain_map[x+y*width] = terrains[i].color;
                        break;
                    }
            }

        renderMap(terrain_map, width, height);
    }

    public void renderTempMap(float[,] noise_map){
        int width = noise_map.GetLength(0);
        int height = noise_map.GetLength(1);

        Color[] temp_map = new Color[width*height];

        Gradient gradient = new Gradient();
        GradientColorKey[] colorkey = new GradientColorKey[2];
        GradientAlphaKey[] alphakey = new GradientAlphaKey[1];

        colorkey[0].color = Color.blue;
        colorkey[0].time = 0f;

        colorkey[1].color = Color.red;
        colorkey[1].time = 1f;

        alphakey[0].alpha = 1.0f;
        alphakey[0].time = 0.0f;

        gradient.SetKeys(colorkey, alphakey);

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                temp_map[x+y*width] = gradient.Evaluate(noise_map[x,y]);
            }

        renderMap(temp_map, width, height);
    }

    public void renderHumidityMap(float[,] noise_map){
        int width = noise_map.GetLength(0);
        int height = noise_map.GetLength(1);

        Color[] temp_map = new Color[width*height];

        Gradient gradient = new Gradient();
        GradientColorKey[] colorkey = new GradientColorKey[2];
        GradientAlphaKey[] alphakey = new GradientAlphaKey[1];

        colorkey[0].color = Color.yellow;
        colorkey[0].time = 0f;

        colorkey[1].color = Color.blue;
        colorkey[1].time = 1f;

        alphakey[0].alpha = 1.0f;
        alphakey[0].time = 0.0f;

        gradient.SetKeys(colorkey, alphakey);

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                temp_map[x+y*width] = gradient.Evaluate(noise_map[x,y]);
            }

        renderMap(temp_map, width, height);
    }

    public void renderMap(Color[] color_map, int width, int height){
        Texture2D texture = new Texture2D(width, height);

        texture.SetPixels(color_map);
        texture.Apply();

        texture_renderer.sharedMaterial.mainTexture = texture;
        texture_renderer.transform.localScale = new Vector3(width,1,height);        
    }

    private Color[] getBiomeColorMap(float[,] temp_map, float[,] hum_map, Biome[] biomes, int[,] biomemap){
        int width = temp_map.GetLength(0);
        int height = temp_map.GetLength(1);

        Color[] biome_map = new Color[width*height];

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                biome_map[x+y*width] = biomes[biomemap[(int)(temp_map[x,y]/0.2f), (int)(hum_map[x,y]/0.2f)]].color;
            }
        
        return biome_map;
    }

    public void renderBiomeTerrainMap(float[,] noise_map, float[,] temp_map, float[,] humidity_map, float[,] falloff_map, bool use_falloff, TerrainType[] terrains, Biome[] biomes, int[,] biomemap){
        int width = noise_map.GetLength(0);
        int height = noise_map.GetLength(1);

        Color[] terrain_map = new Color[width*height];
        Color[] biome_map = getBiomeColorMap(temp_map, humidity_map, biomes, biomemap);

        for(int x = 0; x < width; x++)
            for(int y = 0; y < height; y++){
                if(use_falloff) noise_map[x, y] -= falloff_map[x, y];

                for(int i = 0; i < terrains.Length; i++){
                    if(terrains[i].height >= noise_map[x,y]){
                        terrain_map[x+y*width] = terrains[i].color;
                        break;
                    }
                    if(terrains[i].name == "Sand"){
                        terrain_map[x+y*width] = biome_map[x+y*width];
                        break;
                    }
                }
            }

        renderMap(terrain_map, width, height);
    }

    public void renderMeshBiomeMap(MeshData mesh_data, Texture2D texture, bool update_nav_mesh){
        Mesh mesh = mesh_data.createMesh();
        meshFilter.sharedMesh = mesh;
        meshCollider.sharedMesh = mesh;
		meshRenderer.sharedMaterial.mainTexture = texture;

        mesh = mesh_data.createGMesh();
        grassMeshFilter.sharedMesh = mesh;

        if(update_nav_mesh)
            navMesh.BuildNavMesh();
        
        //navMesh.ignoreNavMeshObstacle = true;
    }

    public void renderBiomeMap(float[,] temp_map, float[,] hum_map, Biome[] biomes, int[,] biomemap){
        int width = temp_map.GetLength(0);
        int height = temp_map.GetLength(0);
        renderMap(getBiomeColorMap(temp_map, hum_map, biomes, biomemap), width, height);
    }

    [System.Serializable]
    public struct TerrainType{
        public string name;
        public float height;
        public Color color;
    }

    [System.Serializable]
    public struct Biome{
        public string Name;
        public Color color;
        public float object_probability;
    }
}
