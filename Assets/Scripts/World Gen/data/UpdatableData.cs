using UnityEngine;
using System.Collections;

public class UpdatableData: ScriptableObject{
    public event System.Action OnValuesUpdated;
    public bool autoUpdate;
    
    protected virtual void OnValidate(){
        if(autoUpdate){
            UnityEditor.EditorApplication.update += NotifyOfUpdatedValue;
            //NotifyOfUpdatedValue();
        }
    }

    public void NotifyOfUpdatedValue(){
        UnityEditor.EditorApplication.update -= NotifyOfUpdatedValue;
        if(OnValuesUpdated != null){
            OnValuesUpdated();
        }
    }
}