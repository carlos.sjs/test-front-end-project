using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class TerrainData: UpdatableData{
    public float height_multiplier;
    public AnimationCurve height_curve;
    public bool use_falloff_map;

    public float uniform_scale = 1;

    public float minHeight {
        get {
            return height_multiplier * uniform_scale * height_curve.Evaluate(0);
        }
    }

    public float maxHeight {
        get {
            return height_multiplier * uniform_scale * height_curve.Evaluate(1);
        }
    }
}