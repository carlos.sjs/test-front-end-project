using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class BiomeData: UpdatableData{
    public bool use_trees;
    public GameObject[] trees;
    public int min_amount_trees;
    public int max_amount_trees;
    public float tree_treshold;

    public bool use_details;
    public GameObject[] details;
    public float detail_density;

    public bool use_grass;
}