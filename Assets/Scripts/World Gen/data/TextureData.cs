using UnityEngine;
using System.Collections;
using System.Linq;

[CreateAssetMenu()]
public class TextureData: UpdatableData{
    const int texture_size = 512;
    const TextureFormat texture_format = TextureFormat.RGB565;

    float saved_minHeight;
    float saved_maxHeight;

    public TerrainLayer[] layers;

    public void ApplyToMaterial(Material material){
        material.SetInt("layer_count", layers.Length);
        material.SetColorArray("tint_colors", layers.Select(x => x.tint).ToArray());
        material.SetFloatArray("start_heights", layers.Select(x => x.start_height).ToArray());
        material.SetFloatArray("blends", layers.Select(x => x.blend).ToArray());
        material.SetFloatArray("tint_strenght", layers.Select(x => x.tint_strenght).ToArray());
        material.SetFloatArray("texture_scales", layers.Select(x => x.texture_scale).ToArray());
        material.SetTexture("textures", build_texture_array(layers.Select(x => x.texture).ToArray()));
        UpdateMeshHeight(material, saved_minHeight, saved_maxHeight);
    }

    public void UpdateMeshHeight(Material material, float minHeight, float maxHeight){
        saved_maxHeight = maxHeight;
        saved_minHeight = minHeight;
        
        material.SetFloat("min_height", minHeight);
        material.SetFloat("max_height", maxHeight);
    }
    
    Texture2DArray build_texture_array(Texture2D[] textures){
        Texture2DArray texture_array = new Texture2DArray(texture_size, texture_size, textures.Length, texture_format, true);

        for(int i = 0; i < textures.Length; i++){
            texture_array.SetPixels(textures[i].GetPixels(), i);
        }
        texture_array.Apply();

        return texture_array;
    }

    [System.Serializable]
    public struct TerrainLayer{
        public Color tint;
        [Range (0,1)]
        public float tint_strenght;
        [Range(0,1)]
        public float start_height;
        [Range(0,1)]
        public float blend;
        public float texture_scale;

        public Texture2D texture;
    };
}