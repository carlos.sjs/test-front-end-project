using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class NoiseData: UpdatableData{
    [Header("Terrain Settings")]
    public float noise_scale;
    public int octaves;
    [Range(0,1)]
    public float persistance;
    public float lacunarity;

    public int seed;

    protected override void OnValidate(){
        if(octaves < 1) octaves = 1;
        if(lacunarity < 1) lacunarity = 1;

        base.OnValidate();
    }
}