using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunDebugger : MonoBehaviour{
    public GameObject player;
    public GameObject enemy;

    public float c_time = 1f;

    private Rigidbody playerRB;

    void Start() {
        playerRB = player.GetComponent<Rigidbody>();
    }

    public void OnDrawGizmos() {
        if(playerRB == null) return;

        GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
        Gizmos.color =  Color.green;
        foreach(GameObject b in bullets){
            Gizmos.DrawLine(b.transform.position, b.transform.position+b.GetComponent<BulletController>().dir*c_time);
            //if(c_time > 0f)
            //Debug.Log("Bullet: " +( b.transform.position+b.GetComponent<BulletController>().dir*c_time));
        }


        Gizmos.color =  Color.cyan;
        Gizmos.DrawLine(player.transform.position, player.transform.position+playerRB.velocity*c_time);

        if(c_time > 0f)
        //Debug.Log("FUCKING IDIOT (player): " + (player.transform.position+playerRB.velocity*c_time));


        if(c_time > 0f) c_time-=Time.deltaTime;
        else c_time = 0f;
    }
}
