using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtackController : MonoBehaviour
{
    private Animator anim;
    private GameObject wepon;
    public float cooldownT = 0f;
    public static int nClicks = 0;
    float lastClickedT = 0;
    private bool sword = false;
    private bool gun = false;
    private GameObject fireP;
    public GameObject bullet;
    public SwordController SC;

    // Start is called before the first frame update
    void Start()
    {
        wepon = gameObject.transform.GetChild(0).gameObject;
        if(wepon.CompareTag("Sword")){
            anim = GetComponent<Animator>();
            lastClickedT = Time.time;
            anim.speed *= 1.5f;
            sword = true;
        }
        else if(wepon.CompareTag("Gun")){
            gun = true;
            fireP = wepon.transform.GetChild(0).gameObject;
        }
    }

    void Update()
    {
        if(Input.GetButtonDown("Fire1")){
            Attack();
        }
        if((Time.time - lastClickedT > .3f) && sword) anim.ResetTrigger("hit");
        
    }

    void Attack(){
        if(sword){
            lastClickedT = Time.time;
            anim.SetTrigger("hit");
        }
        else if(gun){
            GameObject tempBullet = Instantiate(bullet, fireP.transform.position, transform.rotation);
            tempBullet.GetComponent<BulletController>().setParent("Player");
        }
    }

    public void OnAtack(){
        wepon.GetComponent<BoxCollider>().enabled = true; 
    }

    public void OffAtack(){
        wepon.GetComponent<BoxCollider>().enabled = false; 
    }
    
    public void KbOn(){
        SC.kb = true;
    }

    public void KbOff(){
        SC.kb = false;
    }
}
