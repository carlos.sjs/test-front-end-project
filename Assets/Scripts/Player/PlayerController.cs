using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour{

    [SerializeField] private float moveSpeed = 7f;
    public float rotationSpeed = 10f;
    private Vector3 movement;
    private Vector3 pointer;
    
    private Rigidbody rb;

    public float hp = 100f;
    private bool kback = false;
    private float tmer = 0f;
    private float tmer2 = 2f;
    private float tmer3 = 2f;
    private Vector3 kbackVector;
    private Vector3 VDash;
    public bool inv = false;
    private bool canDash = true;
    
    //Image healthBar variable
    public Image LifeBar;

    //Image DashBar variable
    public Image DashBar;


    private void Start(){
        rb = GetComponent<Rigidbody>();
        hp = 100f;
    }

    private void Update() { // Update is called once per frame
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");


        if(horizontalInput!=0f || verticalInput!=0f) rb.drag=0;
        else rb.drag=5;

        movement = new Vector3( (verticalInput * 0.7f) + (horizontalInput * 0.7f) , 0f,(verticalInput * 0.7f) - (horizontalInput * 0.7f)).normalized;
        if(Input.GetButtonDown("Jump") && canDash){
            if(movement != Vector3.zero){
                tmer2 = 0f;
                inv = true;
                tmer3 = 0f;
                canDash = false;
                VDash = new Vector3(movement.x * 30f, rb.velocity.y, movement.z * 30f);
            }
            else{
                tmer2 = 0f;
                inv = true;
                tmer3 = 0f;
                canDash = false;
                float Yrot = transform.rotation.eulerAngles.y;
                Yrot *= Mathf.Deg2Rad;
                VDash = new Vector3(Mathf.Sin(Yrot) * 30f, rb.velocity.y, Mathf.Cos(Yrot)* 30f);
            }
        }
        else if(Input.GetButton("Fire2")){
            pointer = Input.mousePosition;
            pointer.x -= (Screen.width / 2);
            pointer.y -= (Screen.height / 2);
            float radAngle = Mathf.Atan2(pointer.y, pointer.x);
            float degAngle = (-180 / Mathf.PI) * radAngle + 135;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, degAngle, 0), rotationSpeed * 2 * Time.deltaTime);
            Move(movement * moveSpeed * .5f);
            SpeedControl(moveSpeed * .5f);
        }
        else if(movement != Vector3.zero){
            transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(movement),rotationSpeed * Time.deltaTime);
            Move(movement * moveSpeed);
            SpeedControl(moveSpeed);
        }
        else{
            pointer = Input.mousePosition;
            pointer.x -= (Screen.width / 2);
            pointer.y -= (Screen.height / 2);
            float radAngle = Mathf.Atan2(pointer.y, pointer.x);
            float degAngle = (-180 / Mathf.PI) * radAngle + 135;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, degAngle, 0), rotationSpeed * Time.deltaTime);
            //Move(movement * moveSpeed);
        }
        //Debug.Log(rb.velocity.magnitude);

        if(kback){
            tmer+=Time.deltaTime;
            if(tmer<=.2f){
                rb.velocity = kbackVector * 25f;
            }
            else kback = false;
        }

        if(tmer2<=.2f){
            rb.velocity = VDash;
            tmer2 += Time.deltaTime;
        }
        else inv = false;

        if(tmer3 <= 1.5f){
            tmer3 += Time.deltaTime;
        }
        else canDash = true;

        //Fill dash bar with tmer3
        DashBar.fillAmount = tmer3/1.5f;

    }

    void Move(Vector3 direction){
        //rb.velocity = direction;
        rb.AddForce(direction * Time.deltaTime * 300);
    }

    private void SpeedControl(float speed){
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
        
        if(flatVel.magnitude > speed){
            Vector3 limitVel = flatVel.normalized * speed;
            rb.velocity = new Vector3(limitVel.x, rb.velocity.y, limitVel.z);
        }
    }

    public void hit(float dmg, bool kb, Vector3 other){
        //Debug.Log("hit");
        hp-=dmg;
        //Fill health bar with hp
        LifeBar.fillAmount = hp/100f;

        if(kb){
            kback = true;
            kbackVector = (transform.position - other).normalized;
            tmer = 0f;
        }
    }
}
