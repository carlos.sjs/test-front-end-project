using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof (MapGenerator))]
public class MapGeneratorEditor : Editor{
    public override void OnInspectorGUI(){
        MapGenerator map_gen = (MapGenerator) target;

        if(DrawDefaultInspector()){
            if(map_gen.autoUpdate) map_gen.generateMap(false);
        }

        if(GUILayout.Button("Generate")){
            map_gen.generateMap(false);
        }
        if(GUILayout.Button("Clear")){
            Debug.Log("Haha this butos doen't do anything, you pice od dumb shit");
        }
    }
}
